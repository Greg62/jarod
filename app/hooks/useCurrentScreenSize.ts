import { useWindowSize } from "usehooks-ts";
import theme from "tailwindcss/defaultTheme";

export function useCurrentScreenSize() {
  const useWidth = useWindowSize().width;

  if (!useWidth) return null;

  if (useWidth < parseInt(theme.screens.sm)) {
    return "xs";
  } else if (useWidth < parseInt(theme.screens.md)) {
    return "sm";
  } else if (useWidth < parseInt(theme.screens.lg)) {
    return "md";
  } else if (useWidth < parseInt(theme.screens.xl)) {
    return "lg";
  } else {
    return "xl";
  }
}

export function useCompareScreenSize() {
  const currentSize = useCurrentScreenSize();
  const availableSizes = Object.keys(
    theme.screens,
  ) as (keyof typeof theme.screens)[];

  function getSizeIndexes(targetSize: keyof typeof theme.screens) {
    return {
      currentSizeIndex: availableSizes.findIndex(
        (availableSize) => availableSize === currentSize,
      ),
      targetSizeIndex: availableSizes.findIndex(
        (availableSize) => availableSize === targetSize,
      ),
    };
  }

  function isLowerThan(size: keyof typeof theme.screens) {
    const { currentSizeIndex, targetSizeIndex } = getSizeIndexes(size);

    return !!currentSize && currentSizeIndex < targetSizeIndex;
  }

  function isLowerOrEqualThan(size: keyof typeof theme.screens) {
    const { currentSizeIndex, targetSizeIndex } = getSizeIndexes(size);

    return !!currentSize && currentSizeIndex <= targetSizeIndex;
  }

  function isGreaterThan(size: keyof typeof theme.screens) {
    const { currentSizeIndex, targetSizeIndex } = getSizeIndexes(size);

    return !!currentSize && currentSizeIndex > targetSizeIndex;
  }

  function isGreaterOrEqualThan(size: keyof typeof theme.screens) {
    const { currentSizeIndex, targetSizeIndex } = getSizeIndexes(size);

    return !!currentSize && currentSizeIndex >= targetSizeIndex;
  }

  return {
    isLowerThan,
    isLowerOrEqualThan,
    isGreaterThan,
    isGreaterOrEqualThan,
  };
}
