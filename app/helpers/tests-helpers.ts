import theme from "tailwindcss/defaultTheme";

export function setScreenSize(width: keyof typeof theme.screens) {
  window.innerWidth = parseInt(theme.screens[width]);
}
