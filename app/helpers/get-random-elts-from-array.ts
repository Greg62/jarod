export default function getRandomEltsFromArray<T>(
  arr: T[],
  n: number
): {
  index: number;
  elt: T;
}[] {
  const arrayWithIndexes = arr.map((elt, index) => ({ index, elt }));
  const shuffled = arrayWithIndexes.sort(() => 0.5 - Math.random());
  return shuffled.slice(0, n);
}
