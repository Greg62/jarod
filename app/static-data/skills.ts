import { Skill } from "@/app/home/[what]/[skill-wheel]/types";

export const skills: Skill[] = [
  {
    label: "Vanilla Javascript",
    logo: "/images/logo_javascript.png",
    rating: {
      value: 4,
      summary: ["4 years on professional projects", "Node and browser"],
    },
    experience: [
      {
        project: "Assessfirst - 4 years",
        context: "SAAS company developping a solution for Predictive Hiring",
        subcontext: "Frontend developer",
        details: [
          "Development of complex analytics reports using D3js and Vanilla JS ",
          "Setup of a PNPM monorepo",
          "Performance optimization using Webpack",
        ],
      },
      {
        project: "Openclassrooms - 1 year",
        context: "Development school - Bachelor level",
        details: [
          "Development of many training projects in VanillaJS",
          "Personal website in PHP and VanillaJS",
        ],
      },
    ],
  },
  {
    label: "Typescript",
    logo: "/images/logo_typescript.png",
    rating: {
      value: 4,
      summary: [
        "4 years on professional projects",
        "Node and browser",
        "Components library",
      ],
    },
    experience: [
      {
        project: "Assessfirst",
        context: "SAAS company developping a solution for Predictive Hiring",
        subcontext: "Frontend development",
        details: [
          "Development from scratch of an SPA using VueJS 3 and a REST API",
          "Development of a component library packaged for Gitlab private repo.",
          "Migration of a Laravel / blade / Vue2 app to a Laravel / Vue3 app.",
        ],
      },
      {
        project: "ChimR - Unfinished",
        context: "R&D project to discover and practice NextJS",
        subcontext:
          "Personal logos to be created by the user and printed on T-Shirts",
        details: [
          "Use of T3 stack: Next, typescript, tRPC, Prisma, tailwind",
          "Drag and drop library",
        ],
      },
      {
        project: "Horizons - unfinished",
        context: "R&D project to discover and practice ReactNative",
        subcontext: "Hiking guide on mobile using geolocation",
        details: ["Based on Expo", "Usage of Native Base", "MapBox"],
      },
    ],
  },
  {
    label: "VueJS",
    logo: "/images/logo_vue.png",
    rating: {
      value: 4,
      summary: ["4 years on professional projects", "Vue 2 and Vue3"],
    },
    experience: [
      {
        project: "Assessfirst - 4 years",
        context: "SAAS company developping a solution for Predictive Hiring",
        subcontext: "Frontend developer",
        details: [
          "Development of complex analytics reports using D3js and Vanilla JS ",
          "Setup of a PNPM monorepo",
          "Performance optimization using Webpack",
        ],
      },
      {
        project: "Openclassrooms - 1 year",
        context: "Development school - Bachelor level",
        details: [
          "Development of many training projects in VanillaJS",
          "Personal website in PHP and VanillaJS",
        ],
      },
    ],
  },
  {
    label: "React",
    logo: "/images/logo_react.png",
    rating: {
      value: 3,
      summary: [
        "3 personal projects including this one",
        "Used with React Native and Next",
      ],
    },
    experience: [
      {
        project: "Assessfirst",
        context: "SAAS company developping a solution for Predictive Hiring",
        subcontext: "Frontend development",
        details: [
          "Development from scratch of an SPA using VueJS 3 and a REST API",
          "Development of a component library packaged for Gitlab private repo.",
          "Migration of a Laravel / blade / Vue2 app to a Laravel / Vue3 app.",
        ],
      },
      {
        project: "ChimR - Unfinished",
        context: "R&D project to discover and practice NextJS",
        subcontext:
          "Personal logos to be created by the user and printed on T-Shirts",
        details: [
          "Use of T3 stack: Next, typescript, tRPC, Prisma, tailwind",
          "Drag and drop library",
        ],
      },
      {
        project: "Horizons - unfinished",
        context: "R&D project to discover and practice ReactNative",
        subcontext: "Hiking guide on mobile using geolocation",
        details: ["Based on Expo", "Usage of Native Base", "MapBox"],
      },
    ],
  },
  {
    label: "Next",
    logo: "/images/logo_next.png",
    rating: {
      value: 2,
      summary: ["2 personal projects including this one", "Hosting on Vercel"],
    },
    experience: [
      {
        project: "Assessfirst - 4 years",
        context: "SAAS company developping a solution for Predictive Hiring",
        subcontext: "Frontend developer",
        details: [
          "Development of complex analytics reports using D3js and Vanilla JS ",
          "Setup of a PNPM monorepo",
          "Performance optimization using Webpack",
        ],
      },
      {
        project: "Openclassrooms - 1 year",
        context: "Development school - Bachelor level",
        details: [
          "Development of many training projects in VanillaJS",
          "Personal website in PHP and VanillaJS",
        ],
      },
    ],
  },
  {
    label: "PHP",
    logo: "/images/logo_php.png",
    rating: {
      value: 4,
      summary: [
        "2 years on professional projects",
        "Initial training course main technology",
        "Used with Symfony and Laravel",
      ],
    },
    experience: [
      {
        project: "Assessfirst",
        context: "SAAS company developping a solution for Predictive Hiring",
        subcontext: "Frontend development",
        details: [
          "Development from scratch of an SPA using VueJS 3 and a REST API",
          "Development of a component library packaged for Gitlab private repo.",
          "Migration of a Laravel / blade / Vue2 app to a Laravel / Vue3 app.",
        ],
      },
      {
        project: "ChimR - Unfinished",
        context: "R&D project to discover and practice NextJS",
        subcontext:
          "Personal logos to be created by the user and printed on T-Shirts",
        details: [
          "Use of T3 stack: Next, typescript, tRPC, Prisma, tailwind",
          "Drag and drop library",
        ],
      },
      {
        project: "Horizons - unfinished",
        context: "R&D project to discover and practice ReactNative",
        subcontext: "Hiking guide on mobile using geolocation",
        details: ["Based on Expo", "Usage of Native Base", "MapBox"],
      },
    ],
  },
  {
    label: "Laravel",
    logo: "/images/logo_laravel.png",
    rating: {
      value: 3,
      summary: [
        "2 years on professional projects",
        "REST and GraphQL APIs",
        "Monolithic apps with Blade",
      ],
    },
    experience: [
      {
        project: "Assessfirst - 4 years",
        context: "SAAS company developping a solution for Predictive Hiring",
        subcontext: "Frontend developer",
        details: [
          "Development of complex analytics reports using D3js and Vanilla JS ",
          "Setup of a PNPM monorepo",
          "Performance optimization using Webpack",
        ],
      },
      {
        project: "Openclassrooms - 1 year",
        context: "Development school - Bachelor level",
        details: [
          "Development of many training projects in VanillaJS",
          "Personal website in PHP and VanillaJS",
        ],
      },
    ],
  },
  {
    label: "GraphQL",
    logo: "/images/logo_graphql.png",
    rating: {
      value: 3,
      summary: [
        "1 year on a professional project",
        "Backend server with Lighthouse",
        "Frontend client with Apollo",
      ],
    },
    experience: [
      {
        project: "Assessfirst",
        context: "SAAS company developping a solution for Predictive Hiring",
        subcontext: "Frontend development",
        details: [
          "Development from scratch of an SPA using VueJS 3 and a REST API",
          "Development of a component library packaged for Gitlab private repo.",
          "Migration of a Laravel / blade / Vue2 app to a Laravel / Vue3 app.",
        ],
      },
      {
        project: "ChimR - Unfinished",
        context: "R&D project to discover and practice NextJS",
        subcontext:
          "Personal logos to be created by the user and printed on T-Shirts",
        details: [
          "Use of T3 stack: Next, typescript, tRPC, Prisma, tailwind",
          "Drag and drop library",
        ],
      },
      {
        project: "Horizons - unfinished",
        context: "R&D project to discover and practice ReactNative",
        subcontext: "Hiking guide on mobile using geolocation",
        details: ["Based on Expo", "Usage of Native Base", "MapBox"],
      },
    ],
  },
  {
    label: "ChartJS",
    logo: "/images/logo_chartjs.png",
    rating: {
      value: 3,
      summary: [
        "4 years on professional projects",
        "Various type of complex charts",
      ],
    },
    experience: [
      {
        project: "Assessfirst - 4 years",
        context: "SAAS company developping a solution for Predictive Hiring",
        subcontext: "Frontend developer",
        details: [
          "Development of complex analytics reports using D3js and Vanilla JS ",
          "Setup of a PNPM monorepo",
          "Performance optimization using Webpack",
        ],
      },
      {
        project: "Openclassrooms - 1 year",
        context: "Development school - Bachelor level",
        details: [
          "Development of many training projects in VanillaJS",
          "Personal website in PHP and VanillaJS",
        ],
      },
    ],
  },
  {
    label: "D3JS",
    logo: "/images/logo_d3.png",
    rating: {
      value: 2,
      summary: ["1 year on a professional project", "Data visualization"],
    },
    experience: [
      {
        project: "Assessfirst",
        context: "SAAS company developping a solution for Predictive Hiring",
        subcontext: "Frontend development",
        details: [
          "Development from scratch of an SPA using VueJS 3 and a REST API",
          "Development of a component library packaged for Gitlab private repo.",
          "Migration of a Laravel / blade / Vue2 app to a Laravel / Vue3 app.",
        ],
      },
      {
        project: "ChimR - Unfinished",
        context: "R&D project to discover and practice NextJS",
        subcontext:
          "Personal logos to be created by the user and printed on T-Shirts",
        details: [
          "Use of T3 stack: Next, typescript, tRPC, Prisma, tailwind",
          "Drag and drop library",
        ],
      },
      {
        project: "Horizons - unfinished",
        context: "R&D project to discover and practice ReactNative",
        subcontext: "Hiking guide on mobile using geolocation",
        details: ["Based on Expo", "Usage of Native Base", "MapBox"],
      },
    ],
  },
  {
    label: "StoryBook",
    logo: "/images/logo_storybook.png",
    rating: {
      value: 3,
      summary: [
        "1 year on a professional project",
        "Used with Vue components library",
      ],
    },
    experience: [
      {
        project: "Assessfirst - 4 years",
        context: "SAAS company developping a solution for Predictive Hiring",
        subcontext: "Frontend developer",
        details: [
          "Development of complex analytics reports using D3js and Vanilla JS ",
          "Setup of a PNPM monorepo",
          "Performance optimization using Webpack",
        ],
      },
      {
        project: "Openclassrooms - 1 year",
        context: "Development school - Bachelor level",
        details: [
          "Development of many training projects in VanillaJS",
          "Personal website in PHP and VanillaJS",
        ],
      },
    ],
  },
];
