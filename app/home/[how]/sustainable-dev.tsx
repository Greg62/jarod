import Slider from "@/app/components/[slider]/slider";

function SustainableItem({ item }: { item: string }) {
  return (
    <div className="w-64 h-full flex justify-center items-center bg-gray-200 p-4 border-2 border-slate-300 border-solid">
      {item}
    </div>
  );
}

export default function SustainableDev() {
  const items = [
    "Modular",
    "Scalable",
    "Tested",
    "Accessible",
    "Responsive",
    "Maintanable",
    "Performant",
    "SEO friendly",
    "Secure by design",
  ];

  return (
    <Slider
      className="h-64"
      items={items.map((item, index) => (
        <SustainableItem item={item} key={index} />
      ))}
    ></Slider>
  );
}
