import Section from "@/app/components/section";
import SustainableDev from "./sustainable-dev";

export default function HowSection() {
  return (
    <Section className="w-full">
      <h2 className="text-center mb-4">HOW ?</h2>
      <h3>An easy methodology: the sustainable web development.</h3>
      <SustainableDev />
    </Section>
  );
}
