import Section from "@/app/components/section";
import ContrastBlock from "../components/contrast-block";
import SectionTitle from "../components/section-title";
import { twMerge } from "tailwind-merge";

export default function WhoSection({ className }: { className?: string }) {
  return (
    <Section
      className={twMerge([
        className,
        "flex flex-col items-center justify-start md:flex-row-reverse md:justify-between",
      ])}
    >
      <div className="mb-4 flex w-full items-center justify-center md:w-1/2">
        <SectionTitle title="who am i?" subtitle="A Web dev but not only..." />
      </div>

      <ContrastBlock className="w-full md:w-1/2">
        <div className="typo-body flex flex-col items-center justify-center p-4">
          <p>
            What I offer to my customer is a high-standard development
            experience: I mean Sustainable Development.
          </p>
          <p>
            The code I produce is based on a simple concept: customer first.
          </p>
          <p>
            Then, I follow the SOLID philosphy keeping always in mind the
            maintainability and the efficiency.
          </p>
          <p>
            Before being a developper, I ve been working in the 5-star
            hospitality industry and as a Business Manager.
          </p>
          <p>
            These experiences lead me to never leave a problem without a
            solution.
          </p>
          <p>
            You can always rely on me to understand your context and constraints
            and to find the best answer.
          </p>
        </div>
      </ContrastBlock>
    </Section>
  );
}
