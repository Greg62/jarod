"use client";
import Section from "@/app/components/section";
import SkillsWheel from "./[skill-wheel]/skills-wheel";
import { twMerge } from "tailwind-merge";
import ContrastBlock from "@/app/components/contrast-block";
import { useState } from "react";
import { SelectedContext, SetSelectedContext } from "./skills-contexts";
import XPPanel from "./xp-panel";
import { useCompareScreenSize } from "@/app/hooks/useCurrentScreenSize";
import { MobilePanel } from "./mobile-xp-panel";
import { useIsClient } from "usehooks-ts";
import SectionTitle from "@/app/components/section-title";

function TitleBlock() {
  const { isLowerThan } = useCompareScreenSize();
  if (!useIsClient())
    return (
      <SectionTitle
        title="SKILLS WHEEL"
        subtitle="A comprehensive list of my technical stack."
        className="mb-4 flex flex-col items-center justify-center max-md:mb-5"
      />
    );

  if (isLowerThan("md"))
    return (
      <div>
        <SectionTitle
          title="SKILLS WHEEL"
          subtitle="A comprehensive list of my technical stack."
          className="mb-4 flex flex-col items-center justify-center max-md:mb-5"
        />
        <MobilePanel />
      </div>
    );

  return (
    <XPPanel className="mb-5 md:mb-0 md:w-1/2">
      <SectionTitle
        title="SKILLS WHEEL"
        subtitle="A comprehensive list of my technical stack."
        className="mb-4 flex flex-col items-center justify-center max-md:mb-5"
      />
    </XPPanel>
  );
}

export default function WhatSection({ className }: { className?: string }) {
  const [selected, setSelected] = useState<number | null>(null);

  return (
    <SelectedContext.Provider value={selected}>
      <SetSelectedContext.Provider value={setSelected}>
        <Section
          className={twMerge([
            "flex h-[30rem] flex-col items-stretch justify-between md:flex-row md:space-x-4",
            className,
          ])}
        >
          <TitleBlock />

          <ContrastBlock className="md:w-1/2">
            <SkillsWheel width={300} />
          </ContrastBlock>
        </Section>
      </SetSelectedContext.Provider>
    </SelectedContext.Provider>
  );
}
