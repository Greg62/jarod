import { render, screen } from "@testing-library/react";
import { SkillXP } from "./skill-xp";
import { skills } from "@/app/static-data/skills";
import getRandomEltsFromArray from "@/app/helpers/get-random-elts-from-array";

describe("SkillXp", () => {
  beforeEach(() => {
    // As the panel is rendered in a portal, we need to create a div with the id "panel-content"
    render(<div id="panel-content"></div>);
  });

  test("Given an XP description, When rendered, Then should display the XP project", () => {
    // Arrange
    const randomSkill = getRandomEltsFromArray(skills, 1)[0];

    // Act
    render(<SkillXP experiences={randomSkill.elt.experience} />);

    // Assert
    randomSkill.elt.experience.forEach((experience) => {
      expect(screen.getByText(experience.project)).toBeDefined();
    });
  });

  test("Given an XP description, When rendered, Then should display the XP context", () => {
    // Arrange
    const randomSkill = getRandomEltsFromArray(skills, 1)[0];

    // Act
    render(<SkillXP experiences={randomSkill.elt.experience} />);

    // Assert
    randomSkill.elt.experience.forEach((experience) => {
      expect(screen.queryByText(experience.context)).toBeDefined();
    });
  });

  test("Given an XP description, When rendered, Then should display the XP subcontext", () => {
    // Arrange
    const randomSkill = getRandomEltsFromArray(skills, 1)[0];

    // Act
    render(<SkillXP experiences={randomSkill.elt.experience} />);

    // Assert
    randomSkill.elt.experience.forEach((experience) => {
      if (experience.subcontext) {
        expect(screen.queryByText(experience.subcontext)).toBeDefined();
      }
    });
  });

  test("Given an XP description, When rendered, Then should display the XP details", () => {
    // Arrange
    const randomSkill = getRandomEltsFromArray(skills, 1)[0];

    // Act
    render(<SkillXP experiences={randomSkill.elt.experience} />);

    // Assert
    randomSkill.elt.experience.forEach((experience) => {
      experience.details.forEach((detail) => {
        const elt = screen.queryByText(detail, { ignore: true });
        expect(elt).toBeDefined();
      });
    });
  });
});
