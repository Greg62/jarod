import { SkillXP } from "../types";

function XPDetails({ details }: { details: SkillXP["details"] }) {
  return (
    <ul className="list-inside list-disc">
      {details.map((item, index) => (
        <li key={index}>{item}</li>
      ))}
    </ul>
  );
}

export function SkillXP({ experiences }: { experiences: SkillXP[] }) {
  return (
    <ul
      className={`flex flex-col items-start justify-start space-y-2 transition-opacity delay-300 duration-100 ease-in-out md:min-w-[22vw]`}
    >
      {experiences.map((experience, index) => (
        <li
          className="flex flex-col items-start justify-start space-y-1"
          key={index}
        >
          <span className="text-lg font-bold">{experience.project}</span>
          <span className="italic">{experience.context}</span>
          {experience.subcontext && <span>{experience.subcontext}</span>}
          <XPDetails details={experience.details}></XPDetails>
        </li>
      ))}
    </ul>
  );
}
