// In case of flat angle, the rotation is the absolute value of the angle
export default function absoluteFlatAngle(angle: number) {
  if (Math.abs(angle) === 180 || Math.abs(angle) === 0) {
    return Math.abs(angle);
  }

  return angle;
}
