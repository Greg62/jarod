"use client";
import { useState } from "react";
import absoluteFlatAngle from "./absoluteFlatAngle";
import SkillItem from "./[skill-item]/skill-item";
import {
  useSelectedSkill,
  useSetSelectedSkill,
  useSkills,
} from "../skills-contexts";

type SkillsListProps = {
  width?: number;
};

export default function SkillsWheel({ width = 300 }: SkillsListProps) {
  const skillsList = useSkills() || [];
  const selected = useSelectedSkill();
  const setSelected = useSetSelectedSkill();

  const rotationUnit = 360 / skillsList.length;
  const [wheelRotation, setWheelRotation] = useState(0);

  const selectedSkill = typeof selected === "number" && skillsList[selected];

  function rotate(index: number) {
    // If items are located on the top of the wheel, the wheel should rotate clockwise (negative angle)
    // while on the bottom, it should rotate counter clockwise (positive angle)
    const rotation =
      index * rotationUnit + wheelRotation > 180
        ? 360 - index * rotationUnit
        : absoluteFlatAngle(-(index * rotationUnit));

    setWheelRotation(rotation);
  }

  function selectItem(index: number) {
    rotate(index);
    setSelected && setSelected(index);
  }

  return (
    <div
      style={{ width: width }}
      className="h-1/2vh flex items-center justify-center py-12"
    >
      <div className="relative flex w-screen items-center justify-center">
        <div
          data-testid="wheel-container"
          style={{
            width: width,
            transform: `rotate(${wheelRotation}deg)`,
            transition: "transform ease-in 0.5s",
          }}
          className="relative aspect-square w-full rounded-full "
        >
          {skillsList.map((skill, index) => (
            <SkillItem
              id={`skill-item-${index}`}
              key={index}
              itemRotation={index * rotationUnit}
              wheelRadius={width / 2}
              item={skill}
              wheelOffset={wheelRotation}
              onClick={() => selectItem(index)}
              className={
                selectedSkill && selectedSkill.label === skill.label
                  ? "w-12 drop-shadow-emphasize"
                  : "w-9"
              }
            />
          ))}
        </div>
        <div className="absolute left-1/2 top-1/2 flex -translate-x-1/2 -translate-y-1/2 items-center justify-center">
          <h3 className="text-center text-2xl font-bold">
            <span className="typo-subtitle">
              {selectedSkill && selectedSkill.label
                ? selectedSkill.label
                : "Please click on a skill to get details"}
            </span>
          </h3>
        </div>
      </div>
    </div>
  );
}
