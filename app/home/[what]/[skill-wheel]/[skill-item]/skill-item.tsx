import { Skill } from "@/app/home/[what]/[skill-wheel]/types";
import Image from "next/image";
import { twMerge } from "tailwind-merge";

type Props = {
  id: string;
  item: Skill;
  wheelRadius: number;
  itemRotation: number;
  wheelOffset: number;
  className?: string;
  onClick: () => void;
};

export default function SkillItem({
  id,
  item,
  wheelRadius,
  itemRotation,
  wheelOffset,
  className,
  onClick,
}: Props) {
  const xPos = (wheelRadius * Math.cos((itemRotation * Math.PI) / 180)).toFixed(
    2,
  );
  const yPos = (wheelRadius * Math.sin((itemRotation * Math.PI) / 180)).toFixed(
    2,
  );

  const style = {
    transition: "transform ease-in 0.5s",
    transform: `translate(calc(-50% + (${xPos}px)), calc(-50% + (${yPos}px))) rotate(${-wheelOffset}deg)`,
  };

  return (
    <button
      data-testid="skill-btn"
      style={style}
      className={twMerge([
        "absolute left-1/2 top-1/2 flex origin-center cursor-pointer flex-col items-center justify-center text-wrap text-sm",
        className,
      ])}
      onClick={() => onClick()}
    >
      <Image
        id={id}
        width={150}
        height={150}
        src={item.logo}
        alt={item.label}
      />
    </button>
  );
}
