import { render, screen } from "@testing-library/react";
import SkillItem from "./skill-item";
import { skills } from "@/app/static-data/skills";
import getRandomEltsFromArray from "@/app/helpers/get-random-elts-from-array";

describe("SkillItem", () => {
  const wheelRadius = 150;
  const wheelRotation = Math.random() * 360;
  const item = getRandomEltsFromArray(skills, 1)[0];
  const itemRotation = getSkillItemRotation(item.index);

  function getSkillItemRotation(index: number) {
    return (360 / skills.length) * index;
  }

  test("GivenSkillItemWhenRenderLogoIsDisplayed", () => {
    // Act
    render(
      <SkillItem
        id="test"
        item={item.elt}
        itemRotation={itemRotation}
        wheelRadius={wheelRadius}
        wheelOffset={wheelRotation}
        onClick={() => {}}
      />,
    );

    const imageElt = screen.getByRole("img", { name: item.elt.label });

    // Assert
    expect(imageElt).not.toBe(undefined);
  });

  test("GivenItemPositionWhenRenderThenItemLocatedOnCircle", () => {
    // Act
    render(
      <SkillItem
        id="test"
        item={item.elt}
        itemRotation={itemRotation}
        wheelRadius={wheelRadius}
        wheelOffset={wheelRotation}
        onClick={() => {}}
      />,
    );

    const skillElt = screen.getByRole("button");

    function getEltTranslation(elt: HTMLElement) {
      const xRegex = /\s*\(calc\(\s*-50%\s*\+\s*\((.*?)px\)\s*\)/;
      const yRegex = /,\s*calc\(-50%\s*\+\s*\((.*?)px\)\s*\)\)/;

      const xMatch = elt.style.transform.match(xRegex);
      const yMatch = elt.style.transform.match(yRegex);

      return {
        actualX: xMatch && xMatch[1] && parseFloat(xMatch[1]).toFixed(2),
        actualY: yMatch && yMatch[1] && parseFloat(yMatch[1]).toFixed(2),
      };
    }

    // Act

    // Assert
    const angle = (360 / skills.length) * item.index;

    const expectedX = (wheelRadius * Math.cos((Math.PI * angle) / 180)).toFixed(
      2,
    );
    const expectedY = (wheelRadius * Math.sin((Math.PI * angle) / 180)).toFixed(
      2,
    );

    const { actualX, actualY } = getEltTranslation(skillElt);

    expect(actualX).toEqual(expectedX);
    expect(actualY).toEqual(expectedY);
  });

  test("GivenWheelRotatesWhenRenderThenItemCompensatesRotation", () => {
    // Act
    render(
      <SkillItem
        id="test"
        item={item.elt}
        itemRotation={itemRotation}
        wheelRadius={wheelRadius}
        wheelOffset={wheelRotation}
        onClick={() => {}}
      />,
    );

    const skillElt = screen.getByRole("button");

    function getEltRotation(elt: HTMLElement) {
      const eltTransformProperty = elt.style.transform.match(
        /rotate\((-?\d+(?:.\d+?))deg\)/,
      );

      let actualRotation = 0;

      if (eltTransformProperty) {
        actualRotation = parseFloat(eltTransformProperty[1]);
      }

      return actualRotation;
    }

    // Act

    // Assert
    const expectedRotation = -wheelRotation;

    const actualRotation = getEltRotation(skillElt);

    expect(actualRotation).toEqual(expectedRotation);
  });
});
