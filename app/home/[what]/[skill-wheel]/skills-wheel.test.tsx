import { expect, test, describe, beforeEach } from "vitest";
import { render, screen, fireEvent, within } from "@testing-library/react";
import SkillsWheel from "./skills-wheel";
import getRandomEltsFromArray from "@/app/helpers/get-random-elts-from-array";
import absoluteFlatAngle from "./absoluteFlatAngle";
import { skills } from "@/app/static-data/skills";

const width = 300;
const rotationUnit = 360 / skills.length;

function getEltRotation(elt: HTMLElement) {
  const eltTransformProperty = elt.style.transform.match(
    /rotate\((-?\d+(?:.\d+?))deg\)/,
  );

  let actualRotation = 0;

  if (eltTransformProperty) {
    actualRotation = parseFloat(eltTransformProperty[1]);
  }

  return actualRotation;
}

function getSkillsItemsEltsList() {
  const wheelContainer = screen.getByTestId("wheel-container");
  return within(wheelContainer).getAllByRole("button");
}

function getSkillItemElt(skillIndex?: number) {
  const items = getSkillsItemsEltsList();

  return skillIndex
    ? {
        elt: items[skillIndex],
        index: skillIndex,
      }
    : getRandomEltsFromArray(items, 1)[0];
}

function getActualWheelRotation() {
  const wheelElement = screen.getByTestId("wheel-container");
  return getEltRotation(wheelElement);
}

describe("SkillsWheel", () => {
  beforeEach(() => {
    // As the XP panel is rendered in a portal, we need to create a div with the id "panel-content"
    render(<div id="panel-content"></div>);
    render(<SkillsWheel width={width} />);
  });

  test("Given a list of skills When rendered Then all skills are displayed", () => {
    // Act
    const items = getSkillsItemsEltsList();

    // Assert
    expect(items.length).toBe(skills.length);
  });

  test("Given item is on top side of the wheel When item is selected Then wheel rotates clockwise", () => {
    // Arrange
    // Gets the item on top of the wheel (located at 3/4 of the array)
    const eltOnTop = getSkillItemElt(Math.ceil((skills.length * 3) / 4));

    // Act
    fireEvent.click(eltOnTop.elt);

    // Gets the actual wheel rotation angle
    const actualRotation = getActualWheelRotation();

    // Assert
    expect(actualRotation).toBeGreaterThan(0);
  });

  test("Given item is on bottom side of the wheel When item is selected Then wheel rotates counter clockwise", () => {
    // Arrange
    // Gets the item on bottom of the wheel (located at 1/4 of the array)
    const eltOnBottom = getSkillItemElt(Math.ceil(skills.length / 4));

    // Act
    fireEvent.click(eltOnBottom.elt);

    // Gets the actual wheel rotation angle
    const actualRotation = getActualWheelRotation();

    // Assert
    expect(actualRotation).toBeLessThan(0);
  });

  test("Given an item is selected When click on item Then wheel rotates following the shorter angle selected position", () => {
    // Arrange
    const randomSkill = getSkillItemElt();

    // Act
    fireEvent.click(randomSkill.elt);

    // Gets the actual wheel rotation angle
    const actualRotation = getActualWheelRotation();

    // Expected angle (if angle > 180deg, then the rotation should be in the opposite direction)
    const expectedRotation =
      randomSkill.index * rotationUnit > 180
        ? 360 - randomSkill.index * rotationUnit
        : absoluteFlatAngle(-(randomSkill.index * rotationUnit));

    // Assert
    expect(expectedRotation).toEqual(actualRotation);
  });

  test("Given two items selected consecutively When the second one is selected, Then the wheel should rotate the shorter angle to move item to selected position", () => {
    // Act

    // Gets 2 different items.
    const items = getSkillsItemsEltsList();
    const randomItem = getRandomEltsFromArray(items, 2);

    const firstSkill = randomItem[0];
    const secondSkill = randomItem[1];

    fireEvent.click(firstSkill.elt);
    fireEvent.click(secondSkill.elt);

    // Gets the actual wheel rotation angle
    const actualRotation = getActualWheelRotation();

    // Expected angle (if angle > 180deg, then the rotation should be in the opposite direction)
    const firstRotation =
      firstSkill.index * rotationUnit > 180
        ? 360 - firstSkill.index * rotationUnit
        : absoluteFlatAngle(-(firstSkill.index * rotationUnit));

    const secondRotation =
      secondSkill.index * rotationUnit + firstRotation > 180
        ? 360 - secondSkill.index * rotationUnit
        : absoluteFlatAngle(-(secondSkill.index * rotationUnit));

    // Assert
    expect(secondRotation).toEqual(actualRotation);
  });
});
