export type SkillXP = {
  project: string;
  context: string;
  subcontext?: string;
  details: string[];
};

export type Rating = {
  value: 1 | 2 | 3 | 4 | 5;
  summary: string[];
};

export type Skill = {
  label: string;
  logo: string;
  rating: Rating;
  experience: SkillXP[];
};
