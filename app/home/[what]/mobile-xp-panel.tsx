import BottomPanel from "@/app/components/[bottom-panel]/bottom-panel";
import { Skill } from "./[skill-wheel]/types";
import {
  useSelectedSkill,
  useSetSelectedSkill,
  useSkills,
} from "./skills-contexts";
import { SkillXP } from "./[skill-wheel]/[skill-xp]/skill-xp";

export function MobilePanel() {
  const skillsList = useSkills() || [];
  const selected = useSelectedSkill();
  const setSelected = useSetSelectedSkill();

  const visible = typeof selected === "number";

  const title = (
    <h4 className="typo-h4">
      {visible && skillsList[selected] ? skillsList[selected].label : null}
    </h4>
  );

  return (
    <BottomPanel
      title={title}
      show={visible}
      onClose={() => setSelected && setSelected(null)}
    >
      {visible && skillsList[selected] ? (
        <div data-testid="xp-container-mobile">
          <SkillXP experiences={skillsList[selected].experience} />
        </div>
      ) : null}
    </BottomPanel>
  );
}
