import {
  act,
  fireEvent,
  getAllByRole,
  render,
  screen,
  within,
} from "@testing-library/react";
import WhatSection from "./what-section";
import { setScreenSize } from "@/app/helpers/tests-helpers";
import { useScreen } from "usehooks-ts";
import ScreenSizeProvider, {
  useScreenSize,
} from "@/app/components/[providers]/screen-provider";
import { SelectedContext } from "./skills-contexts";

describe("WhatSection", () => {
  test("Given no skill is selected, When skill is selected, Then side-panel panel should be visible", () => {
    // Arrange
    render(
      <SelectedContext.Provider value={null}>
        <WhatSection />
      </SelectedContext.Provider>,
    );

    // Act
    const skillItems = screen.getAllByTestId("skill-btn");
    fireEvent.click(skillItems[0]);

    const panel = screen.getByTestId("xp-container");

    // Assert
    expect(
      panel.className.includes("ml-0") && panel.className.includes("w-[200%]"),
    ).toBeTruthy();
  });

  test("Given a skill is selected When close button is clicked, Then side-panel panel should be hidden ", () => {
    // Arrange
    render(
      <SelectedContext.Provider value={1}>
        <WhatSection />
      </SelectedContext.Provider>,
    );

    // Act
    const panel = screen.getByTestId("xp-container");
    const closeBtn = within(panel).getByRole("button", { name: /close/i });
    fireEvent.click(closeBtn);

    // Assert
    expect(
      panel.className.includes("-ml-[100%]") &&
        panel.className.includes("w-[200%]"),
    ).toBeTruthy();
  });

  test("Given mobile context and no skill is selected, When skill is selected, Then bottom panel should be visible", () => {
    setScreenSize("sm");

    // Arrange
    render(
      <SelectedContext.Provider value={null}>
        <WhatSection />
      </SelectedContext.Provider>,
    );

    // Act
    const skillItems = screen.getAllByTestId("skill-btn");
    fireEvent.click(skillItems[0]);

    // Assert
    const panel = screen.getByRole("dialog");

    // Assert
    expect(panel.className.includes("bottom-0")).toBeTruthy();
    setScreenSize("md");
  });

  test("Given mobile context and skill is selected, When close button is clicked, Then bottom-panel should be hidden", () => {
    setScreenSize("sm");

    // Arrange
    render(
      <SelectedContext.Provider value={1}>
        <WhatSection />
      </SelectedContext.Provider>,
    );

    // Act
    const panel = screen.getByRole("dialog");
    const closeBtn = within(panel).getByRole("button", { name: /close/i });
    fireEvent.click(closeBtn);

    // Assert
    expect(panel.className.includes("-bottom-full")).toBeTruthy();
    setScreenSize("md");
  });
});
