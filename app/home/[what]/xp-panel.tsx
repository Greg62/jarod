import React from "react";
import {
  useSelectedSkill,
  useSetSelectedSkill,
  useSkills,
} from "./skills-contexts";
import { SkillXP } from "./[skill-wheel]/[skill-xp]/skill-xp";
import SlidingBlock from "@/app/components/[sliding-block]/sliding-block";

export default function XPPanel({
  children,
  className,
}: {
  children: React.ReactNode;
  className?: string;
}) {
  const skillsList = useSkills() || [];
  const selected = useSelectedSkill();
  const setSelected = useSetSelectedSkill();

  const visible = typeof selected === "number";

  const panelContent =
    skillsList && visible ? (
      <SkillXP experiences={skillsList[selected].experience} />
    ) : null;

  return (
    <SlidingBlock
      className={className}
      panelContent={panelContent}
      visible={visible}
      onClose={() => setSelected && setSelected(null)}
    >
      <div className="flex h-full w-1/2 flex-col items-center justify-center">
        {children}
      </div>
    </SlidingBlock>
  );
}
