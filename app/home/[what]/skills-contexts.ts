import { Dispatch, SetStateAction, createContext, useContext } from "react";
import { Skill } from "./[skill-wheel]/types";
import { skills } from "@/app/static-data/skills";

export const SkillsContext = createContext<Skill[]>(skills);
export const SelectedContext = createContext<number | null>(null);
export const SetSelectedContext = createContext<Dispatch<
  SetStateAction<number | null>
> | null>(null);

export function useSelectedSkill() {
  return useContext(SelectedContext);
}

export function useSetSelectedSkill() {
  return useContext(SetSelectedContext);
}

export function useSkills() {
  return useContext(SkillsContext);
}
