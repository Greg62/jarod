"use client";
import WhatSection from "./[what]/what-section";
import WhoSection from "./who-section";
import HowSection from "./[how]/how-section";
import Hero from "@/app/header/[hero]/hero";
import React from "react";
import ScreenSizeProvider from "../components/[providers]/screen-provider";

export default function Home() {
  return (
    <main className="relative w-full text-on-neutral ">
      <ScreenSizeProvider>
        <Hero />
        <WhatSection />
        {/* <HowSection /> */}
        <WhoSection className="mb-20 max-md:mt-20" />
      </ScreenSizeProvider>
    </main>
  );
}
