import { render } from "@testing-library/react";
import Logo from "./logo";

describe("Logo", () => {
  test("Given size is sm logo, When rendered Then should match the snapshot", () => {
    const logo = render(<Logo size="sm" />);
    expect(logo).toMatchSnapshot();
  });

  test("Given size is md logo, When rendered Then should match the snapshot", () => {
    const logo = render(<Logo size="md" />);
    expect(logo).toMatchSnapshot();
  });

  test("Given size is lg logo, When rendered Then should match the snapshot", () => {
    const logo = render(<Logo size="lg" />);
    expect(logo).toMatchSnapshot();
  });
});
