type Props = {
  size?: "sm" | "md" | "lg";
};

export default function Logo({ size = "sm" }: Props) {
  const sizes = {
    sm: { font: "text-3xl", container: "h-10 w-5" },
    md: { font: "text-4xl", container: "h-12 w-6" },
    lg: { font: "text-5xl", container: "h-14 w-7" },
  };

  const fontSize = sizes[size].font;
  const containerSize = sizes[size].container;

  return (
    <div role="img" className="relative flex items-center justify-center">
      <div
        className={`${containerSize} absolute right-0 rounded-l-full border-l-2 border-solid  border-sky-950 bg-sky-950`}
      >
        <span className={`${fontSize} font-protest text-transparent`}>X</span>
        <span
          style={{ transform: "translate(-50%, -50%)" }}
          className={`absolute left-full top-1/2 ${fontSize} font-protest`}
        >
          X
        </span>
      </div>
      <div
        className={`${containerSize} absolute left-0 overflow-x-hidden rounded-r-full border-r-2 border-solid  border-sky-950 bg-transparent`}
      >
        <span className={`${fontSize} font-protest text-transparent`}>X</span>
        <span
          style={{ transform: "translate(50%, -50%)" }}
          className={`absolute right-full top-1/2 ${fontSize} font-protest text-sky-950`}
        >
          X
        </span>
      </div>
    </div>
  );
}
