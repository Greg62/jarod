import { useCurrentScreenSize } from "@/app/hooks/useCurrentScreenSize";
import { createContext, useContext, useReducer } from "react";
import theme from "tailwindcss/defaultTheme";

export const ScreenContext = createContext<
  keyof typeof theme.screens | "xs" | null
>(null);

export function useScreenSize() {
  return useContext(ScreenContext);
}

export default function ScreenSizeProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const screenSize = useCurrentScreenSize();

  return (
    <ScreenContext.Provider value={screenSize}>
      {children}
    </ScreenContext.Provider>
  );
}
