import { twMerge } from "tailwind-merge";
import Icon from "@/app/components/[icon]/icon";
import { useEffect, useState } from "react";
import { createPortal } from "react-dom";
import { useIsClient } from "usehooks-ts";

export default function BottomPanel({
  title,
  children,
  show,
  onClose,
}: {
  title: React.ReactNode;
  children: React.ReactNode;
  show: boolean;
  onClose: () => void | null;
}) {
  const [bottomPosition, setBottomPosition] = useState<
    "-bottom-full" | "bottom-0"
  >("-bottom-full");

  useEffect(() => {
    if (show) {
      setBottomPosition("bottom-0");
    } else {
      setBottomPosition("-bottom-full");
    }
  }, [show]);

  if (!useIsClient()) return null;

  return createPortal(
    <aside
      role="dialog"
      aria-labelledby="panel-title"
      className={twMerge(
        "transition-bottom fixed left-0 z-50 min-h-1/2vh w-full rounded-t-lg border-[1px] bg-contrast text-on-neutral duration-300 ease-in-out ",
        bottomPosition,
      )}
    >
      <div className="flex items-center justify-between rounded-t-lg border-b-[1px] bg-contrast p-4 text-orange-500">
        {title}
        <Icon
          name="close"
          className="text-on-neutral"
          onClick={() => onClose()}
        />
      </div>
      <div id="bottom-panel-content" className="p-4 ">
        {children}
      </div>
    </aside>,
    document.body,
  );
}
