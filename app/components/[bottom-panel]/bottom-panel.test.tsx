import { fireEvent, render, screen, within } from "@testing-library/react";
import BottomPanel from "./bottom-panel";

describe("BottomPanel", () => {
  test("Given panel is hidden When visibility changes Then panel should be visible", () => {
    // Arrange
    const { rerender } = render(
      <BottomPanel title="test" onClose={() => {}} show={false}>
        <div>TEST</div>
      </BottomPanel>,
    );

    // Act
    const panel = screen.getByRole("dialog");

    rerender(
      <BottomPanel title="test" onClose={() => {}} show={true}>
        <div>TEST</div>
      </BottomPanel>,
    );

    // Assert
    expect(panel.className).toContain("bottom-0");
  });

  test("Given panel is visible When property changes Then panel should be hidden", () => {
    // Arrange
    const { rerender } = render(
      <BottomPanel title="test" onClose={() => {}} show={true}>
        <div>TEST</div>
      </BottomPanel>,
    );

    // Act
    const panel = screen.getByRole("dialog");

    rerender(
      <BottomPanel title="test" onClose={() => {}} show={false}>
        <div>TEST</div>
      </BottomPanel>,
    );

    // Assert
    expect(panel.className).toContain("-bottom-full");
  });

  test("Given panel is visible When click on close btn Then close event is emitted", async () => {
    // Arrange
    const mockCloseFn = vi.fn(() => {});

    render(
      <BottomPanel title="test" onClose={() => mockCloseFn()} show={true}>
        <div>TEST</div>
      </BottomPanel>,
    );

    // Act
    const panel = screen.getByRole("dialog");
    const closeButton = within(panel).getByRole("button", { name: "close" });

    fireEvent.click(closeButton);

    expect(mockCloseFn).toHaveBeenCalled();
  });
});
