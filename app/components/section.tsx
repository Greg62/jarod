import { twMerge } from "tailwind-merge";

export default function Section({
  children,
  className,
}: {
  children: JSX.Element | JSX.Element[];
  className?: string;
}) {
  return (
    <section
      className={twMerge(["mx-auto w-full max-w-screen-xl pt-12", className])}
    >
      {children}
    </section>
  );
}
