import Image from "next/image";
import RatingStar from "./[rating-star]/rating-star";
import { twMerge } from "tailwind-merge";

export default function Rating({
  value,
  base = 5,
  classname,
}: {
  value: number;
  base?: number;
  classname?: string;
}): JSX.Element {
  // Array that will contain a list of boolean representing if stars are active or not.
  const activeRatingStars: boolean[] = [];

  for (let i = 0; i < base; i++) {
    if (i + 1 <= value) activeRatingStars.push(true);
    else activeRatingStars.push(false);
  }

  return (
    <div
      className={twMerge(["flex items-center justify-center", classname])}
      data-testid="rating-container"
    >
      {activeRatingStars.map((starIsActive, index) => (
        <RatingStar key={`star-${index}`} active={starIsActive} />
      ))}
    </div>
  );
}
