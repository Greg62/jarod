import Icon from "@/app/components/[icon]/icon";

export default function RatingStar({ active }: { active: boolean }) {
  return <Icon name="star" className={active ? "fill text-orange-500" : ""} />;
}
