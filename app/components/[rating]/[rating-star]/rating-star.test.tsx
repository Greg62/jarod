import { render, screen } from "@testing-library/react";
import RatingStar from "./rating-star";

describe("RatingStar", () => {
  test("GivenActivePropsWhenRenderThenStarIsFilled", () => {
    // Arrange
    render(<RatingStar active />);

    const star = screen.getByRole("img");

    const isActive = star.className.includes("fill");

    // Assert
    expect(isActive).toBeTruthy();
  });

  test("GivenStarIsNotActiveWhenRenderThenStarIsNotFilled", () => {
    // Arrange
    render(<RatingStar active={false} />);

    const star = screen.getByRole("img");

    const isActive = star.className.includes("fill");

    // Assert
    expect(isActive).toBeFalsy();
  });

  test("GivenRatingStarWhenRenderThenStarIconIsDisplayed", () => {
    // Arrange
    render(<RatingStar active={false} />);

    const star = screen.getByRole("img");

    const starIconIsUsed = star.innerHTML.includes("star");

    // Assert
    expect(starIconIsUsed).toBeTruthy();
  });
});
