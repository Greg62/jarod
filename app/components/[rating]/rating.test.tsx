import { render, screen } from "@testing-library/react";
import Rating from "./rating";

describe("Rating", () => {
  const base = 4;
  const value = 3;

  test("GivenNoRatingBaseWhenRenderThen5StarsAreDisplayed", () => {
    // Arrange
    render(<Rating value={value} />);

    // Act
    const stars = screen.getAllByRole("img");

    // Assert
    expect(stars).toHaveLength(5);
  });

  test("GivenRatingBaseWhenRenderThenCorrespondingNumberOfStarsAreDisplayed", () => {
    // Arrange
    render(<Rating value={value} base={base} />);

    // Act
    const stars = screen.getAllByRole("img");

    // Assert
    expect(stars).toHaveLength(base);
  });

  test("GivenRatingValueWhenRenderThenCorrespondingNumberOfActiveStarsAreDisplayed", () => {
    // Arrange
    render(<Rating value={value} />);

    // Act
    const stars = screen.getAllByRole("img");
    const activeStars = stars.filter((star) => star.className.includes("fill"));

    // Assert
    expect(activeStars).toHaveLength(value);
  });
});
