import React from "react";
import { twMerge } from "tailwind-merge";

export default function ContrastBlock({
  children,
  className,
}: {
  children: React.ReactNode;
  className: string;
}) {
  return (
    <div
      className={twMerge(
        "flex w-full items-center justify-center rounded-md bg-contrast shadow-lg",
        className,
      )}
    >
      {children}
    </div>
  );
}
