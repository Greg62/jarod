import React from "react";
import { twMerge } from "tailwind-merge";
import Icon from "@/app/components/[icon]/icon";

export default function SlidingBlock({
  children,
  panelContent,
  visible,
  onClose,
  className,
}: {
  children: React.ReactNode;
  panelContent: React.ReactNode;
  visible: boolean;
  onClose?: () => void;
  className?: string;
}) {
  return (
    <div className={twMerge(["overflow-x-hidden", className])}>
      <div
        data-testid="xp-container"
        className={twMerge([
          visible ? "ml-0" : "-ml-[100%]",
          "flex h-full w-[200%] flex-row-reverse items-center justify-between transition-[margin] duration-200",
        ])}
      >
        {children}

        <aside className="bg-primary relative flex h-full w-1/2 items-start justify-center overflow-y-scroll rounded-md p-4 text-on-primary">
          <Icon
            name="close"
            className="absolute right-4 top-4"
            onClick={() => onClose && onClose()}
          ></Icon>
          {panelContent}
        </aside>
      </div>
    </div>
  );
}
