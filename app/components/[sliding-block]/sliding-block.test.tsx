import { render, screen } from "@testing-library/react";
import SlidingBlock from "./sliding-block";

describe("XPPanel", () => {
  const visibilityClasses = [
    ["hidden", "visible", true, "ml-0"],
    ["visible", "hidden", false, "-ml-[100%]"],
  ] as const;

  test.each(visibilityClasses)(
    "Given panel is %s When visibility changes, Then panel should be %s",
    (before, after, visibility, expected) => {
      // Arrange
      let visible = before;
      const panelContent = <span>TEST PANEL</span>;
      const { rerender } = render(
        <SlidingBlock visible={visibility} panelContent={panelContent}>
          TEST
        </SlidingBlock>,
      );

      // Act
      visible = after;
      rerender(
        <SlidingBlock visible={visibility} panelContent={panelContent}>
          TEST
        </SlidingBlock>,
      );

      // Assert
      const panel = screen.getByTestId("xp-container");
      expect(
        panel.className.includes(expected) &&
          panel.className.includes("w-[200%]"),
      ).toBeTruthy();
    },
  );

  test("Given panel is visible When close button is clicked, Then onClose should be emitted", () => {
    // Arrange
    const mockOnClose = vi.fn();
    const panelContent = <span>TEST PANEL</span>;
    render(
      <SlidingBlock
        visible={true}
        panelContent={panelContent}
        onClose={mockOnClose}
      >
        TEST
      </SlidingBlock>,
    );

    // Act
    const closeBtn = screen.getByRole("button");
    closeBtn.click();

    // Assert
    expect(mockOnClose).toBeCalledTimes(1);
  });
});
