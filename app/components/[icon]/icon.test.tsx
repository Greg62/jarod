import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import Icon from "./icon";

describe("Icon", () => {
  test("GivenNamePropsWhenRenderThenCorrespondingIconIsDisplayed", () => {
    // Arrange
    render(<Icon name="star" />);

    // Act
    const icon = screen.getByRole("img");

    const iconMatchedName = icon.innerHTML.includes("star");
    const materialIconsAreUsed = icon.className.includes(
      "material-symbols-outlined"
    );

    // Assert
    expect(iconMatchedName).toBeTruthy();
    expect(materialIconsAreUsed).toBeTruthy();
  });

  test("GivenOnClickPropsWhenRenderThenIconRoleIsButton", () => {
    // Arrange
    render(<Icon name="star" onClick={() => {}} />);

    // Act
    const icon = screen.queryByRole("button");

    // Assert
    expect(icon).not.toBeNull();
  });

  test("GivenNoOnClickPropsWhenRenderThenIconRoleIsImg", () => {
    // Arrange
    render(<Icon name="star" />);

    // Act
    const icon = screen.queryByRole("img");

    // Assert
    expect(icon).not.toBeNull();
  });

  test("GivenIconIsClickableWhenClickedThenOnClickIsCalled", () => {
    // Arrange
    const onClick = vi.fn(() => console.log("clicked"));
    const { getByRole } = render(
      <Icon name="star" onClick={() => onClick()} />
    );

    // Act
    const icon = getByRole("button");
    fireEvent.click(icon);

    // Assert
    expect(onClick).toHaveBeenCalledTimes(1);
  });
});
