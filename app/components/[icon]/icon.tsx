import { iconNames } from "@/app/styles/material/icons-names";
import { twMerge } from "tailwind-merge";

type IconProps = {
  name: (typeof iconNames)[number];
  className?: string;
  onClick?: () => void;
};

export default function Icon({ name, className, onClick }: IconProps) {
  const clickable = typeof onClick === "function";

  return clickable ? (
    <button
      className={twMerge(
        "material-symbols-outlined rounded-full p-2 hover:shadow-lg hover:shadow-sky-700 active:shadow-md",
        className,
      )}
      onClick={onClick}
    >
      {name}
    </button>
  ) : (
    <span
      role="img"
      className={twMerge("material-symbols-outlined", className)}
    >
      {name}
    </span>
  );
}
