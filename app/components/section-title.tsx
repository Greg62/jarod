import { twMerge } from "tailwind-merge";

export default function SectionTitle({
  title,
  subtitle,
  className,
}: {
  title: string;
  subtitle?: string;
  className?: string;
}) {
  return (
    <div
      className={twMerge([
        className,
        "flex flex-col items-center justify-center space-y-3",
      ])}
    >
      <div className="rounded-b-md border-b-2 border-orange-500">
        <h2 className="typo-h2 mb-2 uppercase">{title}</h2>
      </div>
      <span className="typo-subtitle">{subtitle}</span>
    </div>
  );
}
