import Icon from "../[icon]/icon";

export default function SliderBtn({
  direction,
  onClick,
}: {
  direction: "left" | "right";
  onClick: () => void;
}) {
  return (
    <button className="text-2xl" onClick={() => onClick()}>
      <Icon name={direction === "left" ? "arrow_left" : "arrow_right"}></Icon>
    </button>
  );
}
