"use client";
import React, { useState } from "react";
import SliderBtn from "./slider-btn";
import { twMerge } from "tailwind-merge";
import { useSwipeable } from "react-swipeable";

export default function Slider({
  items,
  className,
}: {
  items: JSX.Element[];
  className?: string;
}) {
  const [activeIndex, setActiveIndex] = useState(0);

  const swipeHandlers = useSwipeable({
    onSwipedLeft: () => {
      moveSlider("left");
    },
    onSwipedRight: () => {
      moveSlider("right");
    },
  });

  function moveSlider(direction: "left" | "right") {
    if (direction === "right") {
      setActiveIndex((activeIndex) =>
        activeIndex === 0 ? 1 : activeIndex + 1
      );
    } else if (direction === "left") {
      setActiveIndex((activeIndex) =>
        activeIndex === 0 ? -1 : activeIndex - 1
      );
    }
  }

  function getTranslationOffset() {
    const itemsContainer = document.getElementById("slider-container");
    if (!itemsContainer) return 0;
    return (-activeIndex / items.length) * itemsContainer?.scrollWidth;
  }

  return (
    <div
      className={twMerge([
        "w-full flex justify-between items-stretch",
        className,
      ])}
    >
      <SliderBtn direction="left" onClick={() => moveSlider("right")} />
      <div className="w-full overflow-hidden" {...swipeHandlers}>
        <div
          id="slider-container"
          className="h-full flex items-stretch transition-transform duration-500 ease-in-out"
          style={{
            transform: `translateX(-${getTranslationOffset()}px)`,
          }}
        >
          {items.map((item, index) => (
            <div key={index} className="flex justify-center items-center">
              {item}
            </div>
          ))}
        </div>
      </div>

      <SliderBtn direction="right" onClick={() => moveSlider("left")} />
    </div>
  );
}
