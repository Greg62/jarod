import Logo from "@/app/components/[logo]/logo";

export default function LogoText() {
  return (
    <div className="flex items-center justify-center font-nova text-2xl md:text-3xl">
      <span className="mr-6 text-on-neutral">THE JAROD</span>
      <Logo size="sm" />
      <span className="ml-6 text-on-neutral">EXPERIENCE</span>
    </div>
  );
}
