"use client";
import React, { useState, useEffect } from "react";
import { twMerge } from "tailwind-merge";
import LogoText from "./logo-text";

export default function HeaderBar() {
  const [prevScrollPos, setPrevScrollPos] = useState(0);
  const [visible, setVisible] = useState(true);

  useEffect(() => {
    const handleScroll = () => {
      const currentScrollPos = window.pageYOffset;
      const visible = prevScrollPos > currentScrollPos;

      setPrevScrollPos(currentScrollPos);
      setVisible(visible);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [prevScrollPos]);

  return (
    <header
      className={twMerge([
        "fixed left-0 top-0 z-50 flex h-16 w-screen items-center justify-center bg-neutral px-4 text-white transition-transform duration-300 md:justify-between",
        !visible &&
          "-translate-y-full transform border-b-[1px] border-solid border-sky-900 ",
      ])}
    >
      <div className="mx-auto flex w-full max-w-screen-xl items-center justify-center md:justify-between ">
        <LogoText />
        <nav className="hidden md:block">
          <ul className="flex items-center justify-center space-x-4 text-on-neutral">
            <li>
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/about">About</a>
            </li>
            <li>
              <a href="/contact">Contact</a>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}
