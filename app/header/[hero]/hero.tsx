import Section from "@/app/components/section";
import Image from "next/image";

export default function Hero() {
  return (
    <Section className="bg-primary mb-5 mt-28 flex flex-col items-center justify-start space-y-10 rounded-md p-4 shadow-lg md:flex-row md:justify-between md:space-x-10">
      <div className="flex flex-col items-center justify-center">
        <h1 className="typo-hero mb-4 text-white">
          SUSTAINABLE WEB DEVELOPMENT
        </h1>
        <p className="typo-subtitle text-on-primary">
          Scalable - Frugal - Maintanable
        </p>
      </div>

      <Image
        src={"/images/hero.png"}
        alt="paper boat"
        width={1920}
        height={1047}
        className="w-full rounded-md md:w-1/2"
      />
    </Section>
  );
}
