export const typography = {
  hero: "text-4xl font-nova text-center drop-shadow-md md:text-5xl",
  h1: "text-4xl font-nova my-5",
  h2: "text-3xl font-nova my-4",
  h3: "text-2xl font-raleway my-3",
  h4: "text-xl font-raleway",
  subtitle: "text-lg md:text-xl font-raleway italic",
  body: "text-base font-raleway",
} as const;
