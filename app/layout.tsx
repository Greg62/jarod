import type { Metadata } from "next";
import { Raleway, Nova_Slim } from "next/font/google";
import "./styles/globals.css";
import "./styles/material/material-symbols.css";
import HeaderBar from "./header/[header-bar]/header-bar";
import localFont from "next/font/local";

// Font files can be colocated inside of `app`
const protest = localFont({
  src: "./fonts/ProtestRevolution-Regular.ttf",
  display: "swap",
  variable: "--font-protest",
});

const nova = Nova_Slim({
  weight: "400",
  subsets: ["latin"],
  variable: "--font-nova",
});

const raleway = Raleway({
  subsets: ["latin"],
  variable: "--font-raleway",
});

export const metadata: Metadata = {
  title: "The Jarod Experience",
  description: "Fullstack Web Developer React / Vue / Next / Laravel",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body
        className={`${raleway.variable} ${protest.variable} ${nova.variable} bg-neutral p-4 font-raleway`}
      >
        <HeaderBar />
        {children}
      </body>
    </html>
  );
}
