import type { Config } from "tailwindcss";
import plugin from "tailwindcss/plugin";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        "on-neutral": "var(--on-neutral)",
        "on-primary": "var(--on-primary)",
        neutral: "var(--neutral)",
        contrast: "rgb(var(--contrast))",
        primary: "var(--primary)",
      },
      fontFamily: {
        raleway: ["var(--font-raleway)"],
        protest: ["var(--font-protest)"],
        nova: ["var(--font-nova)"],
      },
      dropShadow: {
        emphasize: [
          "0 10px 8px rgba(0, 0, 0, 0.1)",
          "0 4px 3px rgba(0, 0, 0, 0.2)",
        ],
      },
      minHeight: {
        "1/2vh": "50vh",
      },
      gridTemplateColumns: {
        "panel-hidden": "25% 25% 25% 25% auto",
        "panel-visible": "18% 18% 18% 18% auto",
      },
    },
  },
  plugins: [
    plugin(({ addComponents, theme }) => {
      addComponents({
        ".typo-hero": {
          "@apply text-4xl font-nova text-center drop-shadow-md md:text-5xl":
            {},
        },
        ".typo-h1": { "@apply text-4xl font-nova my-5": {} },
        ".typo-h2": { "@apply text-3xl font-nova my-4": {} },
        ".typo-h3": { "@apply text-2xl font-raleway my-3": {} },
        ".typo-h4": { "@apply text-xl font-semibold font-raleway": {} },
        ".typo-subtitle": {
          "@apply text-lg md:text-xl font-raleway italic": {},
        },
        ".typo-body": { "@apply text-base font-raleway": {} },
      });
    }),
  ],
};
export default config;
